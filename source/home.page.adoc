= Daniel Shumway
:url:
:stylesheet: /styles/homepage.css
:template_dirs: /extensions /templates
:description: My name is Daniel Shumway and I build things on the Internet.
:keywords: 

My name is Daniel Shumway and I build things on the Internet.

Sometimes that takes the form of tooling (usually in Javascript), AI, or games.
Sometimes it's artwork or technical blog posts. Sometimes it's support for and
collaboration with other Open Source projects.

Wherever possible I try to fix problems using ethical software that respects
both developers and users. If you have an interesting idea or need help with
something then I'd love to get in touch with you.

== Find Me Online

[role="badges"]
- link:https://gitlab.com/danshumway[GitLab^, role="gitlab badge"]
- link:https://github.com/danshumway[GitHub^, role="github badge"]
- link:https://patreon.com/danshumway[Patreon^, role="patreon badge"]
- link:https://liberapay.com/danshumway[Liberapay^, role="liberapay badge"]
- link:https://medium.com/@danShumway[Medium^, role="medium badge"]
- link:https://twitter.com/DanielShumway[Twitter^, role="twitter badge"]

== What I'm Working On

[role="badges"]
- link:https://reset-hard.com[Reset Hard^, role="reset-hard badge"]
- link:https://distilledjs.com[Distilled^, role="distilled badge"]
- link:https://gitlab.com/raisethisbarn/raise[Raise^, role="raise badge"]
- link:https://piglet-plays.gitlab.io/ludum-dare-41/[Piglet^, role="piglet badge"]
//Dormouse
//Reset Hard



//Just stick some code right here.
//In fact, imports are exactly what I would want....
//Or... a separate format or something.
//Or... I dunno, just a way to label arbitrary sections somehow.
