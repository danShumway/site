var Raise = require('raise');
var _ = require('lodash');
var path = require('path');

var highlightjs = require('highlight.js');

var puppeteer = require('puppeteer');

//Wrap around Asciidoctor to get proper curly quotes. There's got to be a better way to do this.
var Opal = (function () {
    return global.Opal; //Already defined by AsciiDoctor
}());

(function () {
    var Asciidoctor = Opal.const_get_qualified('::', 'Asciidoctor');
    var Document = Opal.const_get_qualified(Asciidoctor, 'Document');

    /*
     * The dumbest way to get at internals, but it works for now.
     * There must be a better way to do this.
     *
     * TODO: apply this same strategy to section headers and descriptions.
     * - probably expose it as a method that handlebars can call?
     */
    Opal.defn(Document, '$create_converter', (function (base) {

        return function () {
            var converter = base.apply(this, arguments);

            var convert = converter.$convert;
            converter.$convert = function (node, template_name, opts) {

                var sub_quotes = node.$sub_quotes;
                node.$sub_quotes = function () {

                    /*
                     * Quote rules:
                     * - If preceded by whitespace, open quote
                     * - Otherwise, close quote.
                     */
                    var text = arguments[0];
                    text = text.replace(/(^|\s)"/g, '$1“');
                    text = text.replace(/(^|\s)'/g, '$1‘');
                    text = text.replace(/"/g, '”');
                    text = text.replace(/'/g, '’');
                    arguments[0] = text;

                    var result = sub_quotes.apply(this, arguments);

                    /*
                     * Now we swap out the curly quotes with more friendly
                     * characters. This also allows us to handle hanging quotes.
                     *
                     * A hanging quote that is literally the first letter in a
                     * paragraph or section must not be given an extra push
                     * element. This is... frankly, pretty annoying to check.
                     *
                     * But, we all have our own problems to deal with.
                     */
                    result = result.replace(/^‘/, `</span><span class="quote single-open">'</span>`);
                    result = result.replace(/^“/, `</span><span class="quote double-open">"</span>`);

                    result = result.replace(/‘/g, `<span class="single-push"></span><span class="quote single-open">'</span>`);
                    result = result.replace(/’/g, `<span class="quote single-close">'</span>`);
                    result = result.replace(/“/g, `<span class="double-push"></span><span class="quote double-open">"</span>`);
                    result = result.replace(/”/g, `<span class="quote double-close">"</span>`);

                    return result;
                };

                return convert.apply(this, arguments);
            };

            return converter;
        };

    }(Document.$$proto.$create_converter)));

}());


//Raise doesn't support loading .js extensions yet.
//So I've got to cover for it in the meantime.
var extensions = [
    {
        path : 'extensions/document.template.js',
        content : function (ctx) {

            //Loop through sitemap, get list of categories.
            var categories = _.reduce(ctx.sitemap, function (result, file) {
                if (!file.attributes || !file.attributes.category) {
                    return result;
                }

                var collection = file.attributes.category;

                result[collection] = result[collection] || [];
                result[collection].push({
                    title : file.attributes.doctitle,
                    path : path.parse(file.attributes.url).dir,
                    date : new Date(file.attributes.date),
                    formatted_date : new Date(file.attributes.date).toLocaleDateString('en-US', {
                        year : 'numeric',
                        month : 'long',
                        day : 'numeric'
                    }),
                    summary : file.attributes.description
                });

                return result;
            }, {});

            //Sort by date so that you can display them in order. Reverse because newest first.
            _.each(categories, function (category, key) {
                categories[key] = _.reverse(
                    _.sortBy(category, 'date'));
            });

            ctx.node.categories = categories;

            ctx.node.include_toc = ctx.node.attributes.category === 'blog';
            ctx.node.toc = ctx.$node.converter.$convert(ctx.$node, 'outline');

            return ctx.next();
        }
    }, {
        path : 'extensions/listing.template.js',
        content : function (ctx) {
            var lang = ctx.node.attributes.language;
            var content = ctx.node.content();
            var code = highlightjs.highlight(lang, content).value;
            code = code.replace(/&amp;/g, '&'); //Double escape happens.

            return [
                `<div class="listingblock">`,
                `<div class="content"><pre class="highlight">`,
                `<code class="language-${lang}" data-lang="${lang}">${code}</code>`,
                `</pre></div>`,
                `</div>`
            ].join('\n');
        }
    }, {
        path : 'extensions/ulist.template.js',
        content : function (ctx) {
            var roles = Array.from(ctx.node.roles);

            if (roles.length) {
                ctx.node.role = roles.join(' ');
            }
            return ctx.next();
        }
    }, {
        path : 'extensions/open.template.js',
        content : function (ctx) {

            if (ctx.node.style === 'aside') {
                ctx.node.aside = true;
            }

            return ctx.next();
        }
    }, {
        path : 'extensions/paragraph.template.js',
        content : function (ctx) {
            var roles = Array.from(ctx.node.roles);

            if (roles.length) {
                ctx.node.role = roles.join(' ');
            }

            return ctx.next();
        }
    }, {
        path : 'extensions/section.template.js',
        content : function (ctx) {
            ctx.node.hlevel = ctx.node.level + 1;
            return ctx.next();
        }
    }
];


var output = Raise([extensions, './source'], {
    output_dir : './public'

/**-----------RSS-----------------------*/
}).then(function (sitemap) {
    var fs = require('fs');

    var categories = _.reduce(sitemap, function (result, file) {
        if (!file.attributes || !file.attributes.category) {
            return result;
        }

        var collection = file.attributes.category;

        result[collection] = result[collection] || [];
        result[collection].push({
            title : file.attributes.doctitle,
            path : path.parse(file.attributes.url).dir,
            date : new Date(file.attributes.date),
            formatted_date : new Date(file.attributes.date).toLocaleDateString('en-US', {
                year : 'numeric',
                month : 'long',
                day : 'numeric'
            }),
            summary : file.attributes.description
        });

        return result;
    }, {});

    //Sort by date so that you can display them in order. Reverse because newest first.
    _.each(categories, function (category, key) {
        categories[key] = _.reverse(
            _.sortBy(category, 'date'));
    });

    //Manually build template for now.
    var content = (function () {
        var content = `<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<title>Daniel Shumway's Blog</title>
<link>https://danshumway.com</link>
<description>I build things on the Internet, and I post about them here.</description>
<atom:link href="https://danshumway.com/rss.xml" rel="self" type="application/rss+xml" />
<lastBuildDate>${ (new Date).toUTCString() }</lastBuildDate>`;

        _.each(categories['blog'], function (post) {

            content += `<item>
<title>${ post.title }</title>
<link>https://danshumway.com/${ post.path }</link>
<guid>https://danshumway.com/${ post.path }</guid>
<description>${ post.summary }</description>
<pubDate>${ post.date.toUTCString() }</pubDate>
</item>`;
        });

        content += `</channel>
</rss>`;

        return content;
    }());


    return new Promise(function (res, rej) {
        fs.writeFile('./public/rss.xml', content, function (err) {
            if (err) { rej(); return; }
            res();
        });
    });

}).then(async function () {
    var http = require('http');
    var Static = require('node-static');

    var staticServer = new Static.Server('./public', {
        cache : 0
    });

    var server = await new Promise(function (res, rej) {

        var server = http.createServer(function (request, response) {
            request.addListener('end', function () {
                staticServer.serve(request, response, function (err, result) {
                    if (err) {
                        response.writeHead(err.status, err.headers);
                        response.end();
                    }
                });
            }).resume();
        }).listen(8003, 'localhost', function () {
            res(server);
        });
    });

    //Generate pdfs for all of my resumes
    console.log('launching puppeteer');
    var browser = await puppeteer.launch();
    var page = await browser.newPage();

    await page.goto('http://localhost:8003/resume/base/?noheader', { waitUntil: 'networkidle2' });

    console.log('generating PDF');
    await page.pdf({
        // scale : 0.8,
        scale: 1,

        path: './public/resume/base/danshumway-resume.pdf',
        printBackground: true,
        format: 'A3'
    });

    await browser.close();
    server.close();

}).catch(function (err) {
    console.log(err);
});
