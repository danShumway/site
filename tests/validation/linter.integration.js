var fs = require('fs');
var path = require('path');
var globby = require('globby');
var htmlLint = require('htmllint');

/* See https://github.com/htmllint/htmllint/wiki/Options */
var LINT_OPTS = {
    "indent-width": false,

    "class-style": 'none', //should be dash, but syntax highlighter uses underscores :/
    "id-class-style": 'dash'
};

/*
 * Recursively lints every html page under the `public directory`
 */
module.exports = function (options) {
    var {root, suite} = options;

    // var harness = suite.test('HTML linting', async function (suite) {

    //     var paths = await globby('**/*.html', { cwd : path.resolve(root, 'public')});

    //     var files = paths.map(function (filePath) {
    //         filePath = path.resolve(root, 'public', filePath);

    //         return new Promise(function (res, rej) {
    //             fs.readFile(filePath, 'utf8', function (err, data) {
    //                 if (err) { rej(err); }
    //                 res(data);
    //             });
    //         });
    //     });

    //     var pages = await Promise.all(files);
    //     pages.forEach(function (html, index) {

    //         suite.test(paths[index], async function (suite) {
    //             var results = await htmlLint(html, LINT_OPTS);

    //             results.forEach(function (issue) {
    //                 var location = `${issue.code} [${issue.column},${issue.line}]`;
    //                 var error = `${issue.rule} - ${ JSON.stringify(issue.data) }`;
    //                 suite.test(`${location}: ${error}`, false);
    //             });
    //         });
    //     });
    // });
};
