/*-----------------------------------------------------------------------------------------------
 * TEST RUNNER
 *------------------------------------------------------------------------------------------------
 *
 * Sets up a test server, phantomJS instance, and webdriver connector,
 * then loads/runs all integration tests.
 *
 * Using PhantomJS for now just because it requires relatively little setup.
 * It _might_ be a good idea to switch to Headless Chrome later.
 *
 * No matter what you switch to, you should make sure it can be cached locally
 * (ie just like PhantomJS is. Remember that we're aiming for no external dependencies in build)
 *------------------------------------------------------------------------------------------------
 */

/*
 * Non-relative paths are probably not essential,
 * but they give me a little bit of peace of mind.
 */
var path = require('path');
var root = (function () {
    return path.dirname(
        require('find-up').sync('package.json'));
}());

var testroot = path.resolve(root, 'tests');
const TESTS = (function () {
    var globby = require('globby');

    return {
        units : globby.sync('**/*.unit.js', { cwd : testroot }),
        integrations : globby.sync('**/*.integration.js', { cwd : testroot }),
        browsers : globby.sync('**/*.browser.js', { cwd : testroot })
    };
}());

async function units (suite) {
    var tests = TESTS.units;

    tests.forEach(function (testPath) {
        var test = require(path.resolve(testroot, testPath));
        var subSuite = suite.test(testPath);

        test({
            root : root,
            suite : subSuite
        });
    });
}

//TODO: what is, strictly speaking, the difference between setup for a unit/integration test?
//I guess that integrations require a build to be run first.
async function integrations (suite) {
    var tests = TESTS.integrations;

    tests.forEach(function (testPath) {
        var test = require(path.resolve(testroot, testPath));
        var subSuite = suite.test(testPath);

        test({
            root : root,
            suite : subSuite
        });
    });
}

var browsers = (function () {
    var phantomjs = require('phantomjs-prebuilt');
    var webdriverio = require('webdriverio');

    return async function browsers (suite) {

        async function run (testPath, port) {

            var browser = await phantomjs.run(`--webdriver=${port}`);
            var client = webdriverio.remote({
                desiredCapabilities : {
                    browserName : 'phantomjs'
                },

                host : 'localhost',
                port : port
            });

            /*
             * Normally this is something you'd do in-test, but...
             * I dunno, it's just annoying and weird, and I'd much rather do everything here.
             */
            await client.init();

            var test = require(path.resolve(testroot, testPath));
            var subSuite = suite.test(testPath);

            test({
                root : root,
                client : client,
                suite : subSuite
            });

            /*
             * Ends the browser session.
             *
             * Ctrl-C on the process will accomplish the same thing as the below lines
             * so this isn't to prevent rogue phantomJS processes or anything.
             * It's just for Node's benefit.
             */
            subSuite.then(async function () {
                await client.end();
                browser.kill();
            });
        }

        var port = 4448;
        var tests = TESTS.browsers;
        for (var i = 0; i < tests.length; i++) {
            await run(tests[i], port++);

        }
    };
}());

/*
 * Starts up site, runs tests.
 *
 * Would be nice to find a non-callback way to do this
 * but it's just not worth the effort.
 */
(function () {
    var http = require('http');
    var Static = require('node-static');
    var Distilled = require('@latinfor/distilled');

    var staticServer = new Static.Server(path.resolve(root, 'public'), {
        cache : 0 //Caching just seems like a really bad idea for a browser test
    });

    var server = http.createServer(function (request, response) {
        request.addListener('end', function () {
            staticServer.serve(request, response, function (err, result) {
                if (err) {
                    response.writeHead(err.status, err.headers);
                    response.end();
                }
            });
        }).resume();
    }).listen(8000, 'localhost', function () {

        /*
         * Actually running tests.
         * Distilled itself ends up composing most of the testing harness.
         * This just makes managing async code easier - there are fewer things to keep track of.
         */
        var suite = new Distilled();
        suite.test('Units', units);
        suite.test('Integrations', integrations);
        suite.test('Browsers', browsers);

        suite.then(function () {
            server.close();
        });
    });
}());

/* Node still hasn't turned this behavior on by default */
process.on('unhandledRejection', function (up) { throw up; process.exit(1); });
