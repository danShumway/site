var assert = require('assert');

module.exports = function (options) {
    var {root, client, suite} = options;


    /*
     * For safety's sake (and a little bit of performance)
     * Run all tests in series.
     *
     * There's probably a better way to do this,
     * but there's no reason to go crazy for something so trivial right now.
     */
    suite.test('Page connection', async function () {
        await client.url('http://localhost:8000');

    }).test('Basic HTML', async function () {

        var title = await client.getTitle();
        assert.strictEqual(title, 'Daniel Shumway');

    });
};
